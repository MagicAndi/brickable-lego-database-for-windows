﻿using System;
using System.IO;

using CommandLine;

namespace BuildBrickablesDatabase
{
    class Program
    {
        #region Private Data

        private static DateTime startTime;

        #endregion

        static void Main(string[] args)
        {
            Initialize();
            Database database;

            Parser.Default.ParseArguments<CommandLineOptions>(args)
                   .WithParsed<CommandLineOptions>(o =>
            {
                var databaseFilePath = string.Empty;

                if (string.IsNullOrEmpty(o.OutputFile))
                {
                    var currentLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    databaseFilePath = currentLocation + @"\bricks.db";
                }
                else
                {
                    databaseFilePath = o.OutputFile;
                }

                Console.WriteLine("Database file path: '" + databaseFilePath + "'.");
                database = new Database(databaseFilePath, o.DownloadData);
                database.Create();

                Console.WriteLine("Successfully created Brick database at '" + databaseFilePath + "'.");
            });

            Shutdown();
        }

        private static TimeSpan GetElapsedRunningTime()
        {
            var currentTime = DateTime.Now;
            return currentTime.Subtract(startTime);
        }

        private static void Initialize()
        {
            // Add a Global exception handler
            // AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            startTime = DateTime.Now;
            var startup = string.Format("BuildBrickablesDatabase application started at {0}.",
                                        startTime.ToString());
            Console.WriteLine(startup);
        }

        private static void Shutdown()
        {
            // TODO: - Delete all downloaded files, etc

            var elapsedTime = GetElapsedRunningTime();
            var shutdown = string.Format("Program completed at {0} after running for {1:0} seconds.",
                                            DateTime.Now.ToString("HH:mm:ss dd/MM/yyyy"),
                                            elapsedTime.TotalSeconds);
            Console.WriteLine(shutdown);
        }

        /// <summary>
        /// Method to trap unhandled exceptions.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="UnhandledExceptionEventArgs"/> instance containing the event data.</param>
        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine("An unhandled exception has occurred: " + ((Exception)e.ExceptionObject).Message);
            Environment.Exit(1);
        }
    }
}