﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

using CsvHelper;

using BuildBrickablesDatabase.Model;
using BuildBrickablesDatabase.Utilities;
using System.Dynamic;

namespace BuildBrickablesDatabase
{
    internal class Database
    {
        #region Constants 

        private const string rebrickableRootUrl = "https://cdn.rebrickable.com/media/downloads/";

        #endregion Constants

        #region Private Data

        private string databaseFilePath;

        #endregion Private Data

        #region Public Properties

        public string DatabaseDirectory
        {
            get
            {
                return Path.GetDirectoryName(databaseFilePath);
            }
        }

        public string TablesFolder
        {
            get
            {
                return Path.GetDirectoryName(databaseFilePath) + @"\Tables";
            }
        }

        public bool ForceDownload { get; set; }

        #endregion Public Properties

        #region Constructor(s)

        public Database(string databasePath, bool forceDownload)
        {
            databaseFilePath = databasePath;
            ForceDownload = forceDownload;
        }

        #endregion Constructor(s)

        #region Public Methods

        public void Create()
        {
            Console.WriteLine(databaseFilePath);

            if (File.Exists(databaseFilePath))
            {
                File.Delete(databaseFilePath);
            }

            SQLiteConnection.CreateFile(databaseFilePath);

            DownloadTableData();
            CreateDatabaseSchema();
            PopulateDatabaseTables();
            ApplyIndices();
        }


        #endregion Public Methods 

        #region Private Methods
        private void CreateDatabaseSchema()
        {
            SqliteHelper.ExecuteNonQuery(databaseFilePath, SqlScripts.schema);
        }

        private void ApplyIndices()
        {
            SqliteHelper.ExecuteNonQuery(databaseFilePath, SqlScripts.indices);
        }

        private void PopulateDatabaseTables()
        {
            var tableNames = GetTableNames();
            var recordCount = 0;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (var tableName in tableNames)
            {
                Console.WriteLine("Populating table " + tableName);
                recordCount += PopulateTable(tableName);
                Console.WriteLine("Successfully populated table " + tableName);
            }

            stopwatch.Stop();
            var details = string.Format("Inserted {0} records in {1:0} seconds",
                                            recordCount, stopwatch.Elapsed.TotalSeconds);
            Console.WriteLine(details);
        }

        private int PopulateTable(string tableName)
        {
            var csvFile = Path.Combine(TablesFolder, tableName + ".csv");
            int numberOfRecords = 0;

            using (var reader = new StreamReader(csvFile))
            {
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var records = GetRecords(csv, tableName);
                    // Console.WriteLine("Number of records in " + tableName + ".csv: " + records.Count().ToString());
                    SQLiteConnection connection = new SQLiteConnection("Data Source=" + databaseFilePath + ";Version=3;");
                    connection.Open();

                    var bulkInsert = new SqliteBulkInsert(connection, tableName);
                    var firstRecord = true;

                    foreach (var record in records)
                    {
                        if (firstRecord)
                        {
                            Console.WriteLine("Type of first record: " + record.GetType().ToString());
                            Console.WriteLine(firstRecord.ToString());
                            var parameters = records.First().GetParameters();
                            bulkInsert.AddParameters(parameters);
                            firstRecord = false;
                        }

                        bulkInsert.Insert(record.GetValueArray());
                    }

                    numberOfRecords = records.Count();
                    bulkInsert.Flush();
                    connection.Close();
                }
            }

            return numberOfRecords;
        }
       

        private IEnumerable<IBaseItem> GetRecords(CsvReader csvReader, string tableName)
        {
            switch (tableName)
            {
                case "themes":
                    return csvReader.GetRecords<Themes>();
                    break;
                case "colors":
                    return csvReader.GetRecords<Colors>();
                    break;
                case "part_categories":
                    return csvReader.GetRecords<PartCategories>();
                    break;
                case "parts":
                    return csvReader.GetRecords<Parts>();
                    break;
                case "part_relationships":
                    return csvReader.GetRecords<PartRelationships>();
                    break;
                case "elements":
                    return csvReader.GetRecords<Elements>();
                    break;
                case "sets":
                    return csvReader.GetRecords<Sets>();
                    break;
                case "minifigs":
                    return csvReader.GetRecords<Minifigs>();
                    break;
                case "inventories":
                    return csvReader.GetRecords<Inventories>();
                    break;
                case "inventory_parts":
                    return csvReader.GetRecords<InventoryParts>();
                    break;
                case "inventory_sets":
                    return csvReader.GetRecords<InventorySets>();
                    break;
                case "inventory_minifigs":
                    return csvReader.GetRecords<InventoryMinifigs>();
                    break;
                default:
                    throw new Exception("Unknown table name passed!!");
                    break;
            }
        }

        private void DownloadTableData()
        {
            var tableNames = GetTableNames();
            var extension = ".csv.gz";

            if (!Directory.Exists(TablesFolder))
            {
                Directory.CreateDirectory(TablesFolder);
            }

            using (var wc = new CustomWebClient())
            {
                // Set timeout for web client to be 3 minutes
                wc.Timeout = 5 * 60 * 60 * 1000;

                foreach (var tableName in tableNames)
                {
                    if (!File.Exists(Path.Combine(TablesFolder, tableName + ".csv")) || ForceDownload)
                    {
                        Console.WriteLine("Downloading data for table '" + tableName + "'.");
                        var filePath = Path.Combine(TablesFolder, tableName + extension);
                        var url = rebrickableRootUrl + tableName + extension;
                        wc.DownloadFile(url, filePath);
                        ExtractCsvFile(filePath, TablesFolder);
                        Console.WriteLine("Successfully downloaded and extracted data for table '" + tableName + "'.");
                    }                    
                }
            }
        }

        private void ExtractCsvFile(string archiveFilePath, string outputFolder)
        {
            var executable = @"C:\Program Files\7-Zip\7z.exe";

            try
            {
                var process = new ProcessStartInfo();
                process.WindowStyle = ProcessWindowStyle.Hidden;
                process.FileName = executable;
                process.Arguments = "x " + archiveFilePath + " -aoa -o" + outputFolder;
                Process x = Process.Start(process);
                x.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to extract table data from file: " + archiveFilePath + ", error: " + ex.Message);
            }
        }

        private List<string> GetTableNames()
        {
            var tableNames = new List<string>();
            tableNames.Add("themes");
            tableNames.Add("colors");
            tableNames.Add("part_categories");
            tableNames.Add("parts");
            tableNames.Add("part_relationships");
            tableNames.Add("elements");
            tableNames.Add("sets");
            tableNames.Add("minifigs");
            tableNames.Add("inventories");
            tableNames.Add("inventory_parts");
            tableNames.Add("inventory_sets");
            tableNames.Add("inventory_minifigs");

            return tableNames;
        }

        #endregion Private Methods
    }
}