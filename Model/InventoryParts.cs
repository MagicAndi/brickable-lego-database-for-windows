﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class InventoryParts : BaseItem
    {
        public int inventory_id { get; set; }
        public string part_num { get; set; }
        public Int16 color_id { get; set; }
        public Int16 quantity { get; set; }
        public string is_spare { get; set; }
    }
}
