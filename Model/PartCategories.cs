﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class PartCategories : BaseItem
    {
        public Int16 id { get; set; }
        public string name { get; set; }
    }
}
