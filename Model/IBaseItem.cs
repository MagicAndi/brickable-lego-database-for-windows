﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public interface IBaseItem
    {
        string ToString();
        Dictionary<string, DbType> GetParameters();

        object[] GetValueArray();
    }
}