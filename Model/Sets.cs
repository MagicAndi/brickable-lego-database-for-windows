﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class Sets : BaseItem
    {
        public string set_num { get; set; }
        public string name { get; set; }
        public Int16 year { get; set; }
        public Int16 theme_id { get; set; }
        public int num_parts { get; set; }
    }
}