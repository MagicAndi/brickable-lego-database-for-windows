﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class Parts : BaseItem
    {
        public string part_num { get; set; }
        public string name { get; set; }
        public Int16 part_cat_id { get; set; }
        public Int16 part_material_id { get; set; }
    }
}
