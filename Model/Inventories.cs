﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class Inventories : BaseItem
    {
        public int id { get; set; }
        public Int16 version { get; set; }
        public string set_num { get; set; }
    }
}