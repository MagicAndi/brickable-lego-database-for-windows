﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class PartRelationships : BaseItem
    {
        public string rel_type { get; set; }
        public string child_part_num { get; set; }
        public string parent_part_num { get; set; }
    }
}