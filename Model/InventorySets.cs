﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class InventorySets : BaseItem
    {
        public int inventory_id { get; set; }
        public string set_num { get; set; }
        public Int16 quantity { get; set; }
    }
}