﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class Colors : BaseItem
    {
        public Int16 id { get; set; }
        public string name { get; set; }
        public string rgb { get; set; }
        public string is_trans { get; set; }
    }
}