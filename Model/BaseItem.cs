﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace BuildBrickablesDatabase.Model
{    public abstract class BaseItem : IBaseItem
    {
        #region Constructor

        public BaseItem()
        {
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            var values = new List<string>();

            foreach (var prop in this.GetType().GetProperties())
            {
                var propertyType = prop.PropertyType;

                if ((propertyType.GetInterface("IEnumerable") != null) &&
                    (propertyType != typeof(string)))
                {
                    continue;
                }

                var propertyName = prop.Name;
                var propertyValue = prop.GetValue(this, null);
                if (propertyValue == null)
                {
                    propertyValue = "NULL";
                }

                values.Add(string.Format("{0} = {1}", propertyName, propertyValue));
            }

            return string.Join<string>(", ", values);
        }

        public Dictionary<string, DbType> GetParameters()
        {
            var parameters = new Dictionary<string, DbType>();
            DbType dbType;

            foreach (var prop in this.GetType().GetProperties())
            {
                var propertyType = prop.PropertyType;
                Console.WriteLine("Property Type: " + propertyType.ToString());

                switch (propertyType.ToString())
                {
                    case "System.Int16":
                    case "System.Nullable`1[System.Int16]":
                        dbType = DbType.Int16;
                        break;
                    case "System.Int32":
                        dbType = DbType.Int32;
                        break;
                    case "System.String":
                        dbType = DbType.String;
                        break;
                    default:
                        throw new Exception("Unknown property type!");
                        break;
                }

                parameters.Add(prop.Name, dbType);
            }

            return parameters;
        }

        public object[] GetValueArray()
        {
            var length = this.GetType().GetProperties().Length;
            var values = new object[length];
            int index = 0;

            foreach (var prop in this.GetType().GetProperties())
            {
                var propertyType = prop.PropertyType;
                var propertyValue = prop.GetValue(this, null);

                if (propertyValue == null)
                {
                    switch (propertyType.ToString())
                    {
                        case "System.Nullable`1[System.Int16]":
                            propertyValue = 0;
                            break;
                        case "String":
                            propertyValue = "";
                            break;
                        default:
                            throw new Exception("Unknown property type!");
                            break;
                    }
                }

                values[index] = propertyValue;
                index++;
            }

            return values;
        }

        #endregion
    }
}