﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class Elements : BaseItem
    {
        public string element_id { get; set; }
        public string part_num { get; set; }
        public Int16 color_id { get; set; }
    }
}
