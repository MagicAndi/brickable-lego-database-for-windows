﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class Minifigs : BaseItem
    {
        public string fig_num { get; set; }
        public string name { get; set; }
        public Int16 num_parts { get; set; }
    }
}
