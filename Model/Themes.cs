﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuildBrickablesDatabase.Model
{
    public class Themes : BaseItem
    {
        public Int16 id { get; set; }
        public string name { get; set; }
        public Int16? parent_id { get; set; }
    }
}
