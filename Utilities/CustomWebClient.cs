﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace BuildBrickablesDatabase.Utilities
{
    internal class CustomWebClient: System.Net.WebClient
    {
        public int Timeout { get; set; }

        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest lWebRequest = base.GetWebRequest(uri);
            lWebRequest.Timeout = Timeout;
            ((HttpWebRequest)lWebRequest).ReadWriteTimeout = Timeout;
            return lWebRequest;
        }
    }
}
