﻿using System.Data.SQLite;

namespace BuildBrickablesDatabase.Utilities
{
    internal static class SqliteHelper
    {
        public static void ExecuteNonQuery(string databaseFilePath, string sql)
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=" + databaseFilePath + ";Version=3;");
            connection.Open();
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
}
