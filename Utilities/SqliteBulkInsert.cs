﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;

namespace BuildBrickablesDatabase.Utilities
{
    /// <summary>
    /// Class to allow bulk insert statements in SQLite when using System.Data.Sqlite
    /// </summary>
    /// <remarks>
    /// Code based on http://procbits.com/2009/09/08/sqlite-bulk-insert
    /// </remarks>
    internal class SqliteBulkInsert
    {
        #region Constants

        private const string delimiter = ":";

        #endregion

        #region Private Data

        private SQLiteConnection connection;
        private SQLiteCommand command;
        private SQLiteTransaction transaction;
        private int commandCount = 0;

        #endregion Private Data

        #region Public Properties
        
        public string TableName { get; }
        public Dictionary<string, SQLiteParameter> Parameters { get; set; }
        public bool AllowBulkInsert { get; set; }
        public int CommitMax { get; set; }

        #endregion Public Properties

        #region Constructor

        public SqliteBulkInsert(SQLiteConnection sqliteConnection, string tableName)
        {
            connection = sqliteConnection;
            TableName = tableName;
            Parameters = new Dictionary<string, SQLiteParameter>();
            CommitMax = 1000;
        }

        #endregion Constructor

        #region Public Methods
        
        public void Flush()
        {
            try
            {
                if (transaction != null)
                {
                    transaction.Commit();
                }
            }
            catch (Exception ex) 
            { 
                throw new Exception("Could not commit transaction. See InnerException for more details", ex); 
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                }

                transaction = null;
                commandCount = 0;
            }
        }

        public void Insert(object[] parameterValues)
        {
            if (parameterValues.Length != Parameters.Count)
            {
                throw new Exception("The values array count must be equal to the count of the number of parameters.");
            }

            commandCount++;

            if (commandCount == 1)
            {
                if (AllowBulkInsert)
                {
                    transaction = connection.BeginTransaction();
                }

                command = connection.CreateCommand();
                foreach (SQLiteParameter parameter in Parameters.Values)
                {
                    command.Parameters.Add(parameter);
                }

                command.CommandText = BuildCommandText();
            }

            int i = 0;
            foreach (SQLiteParameter parameter in Parameters.Values)
            {
                parameter.Value = parameterValues[i];
                i++;
            }

            command.ExecuteNonQuery();

            if (commandCount == CommitMax)
            {
                try
                {
                    if (transaction != null)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception ex) 
                {
                    Console.WriteLine("Error on trying to commit: " + ex.Message);
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                        transaction = null;
                    }

                    commandCount = 0;
                }
            }
        }

        public void AddParameter(string name, DbType dbType)
        {
            SQLiteParameter parameter = new SQLiteParameter(delimiter + name, dbType);
            Parameters.Add(name, parameter);
        }

        public void AddParameters(Dictionary<string, DbType> parameters)
        {
            foreach (var parameter in parameters)
            {
                AddParameter(parameter.Key, parameter.Value);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private string BuildCommandText()
        {
            if (Parameters.Count < 1)
            {
                throw new SQLiteException("You must add at least one parameter.");
            }

            var commandText = new StringBuilder(255);
            commandText.Append("INSERT INTO [");
            commandText.Append(TableName);
            commandText.Append("] (");

            foreach (string parameterName in Parameters.Keys)
            {
                commandText.Append('[');
                commandText.Append(parameterName);
                commandText.Append(']');
                commandText.Append(", ");
            }

            commandText.Remove(commandText.Length - 2, 2);
            commandText.Append(") VALUES (");

            foreach (string parameterValue in Parameters.Keys)
            {
                commandText.Append(delimiter);
                commandText.Append(parameterValue);
                commandText.Append(", ");
            }

            commandText.Remove(commandText.Length - 2, 2);
            commandText.Append(")");

            return commandText.ToString();
        }

        #endregion Private Methods
    }
}
