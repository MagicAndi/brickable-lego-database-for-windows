# Brickable Lego Database for Windows

A Dot Net Core console app that reates a SQLite database from the Rebrickable Lego database (https://rebrickable.com/downloads/) on a Windows PC.  Based on the superior project from Jon Craton (https://github.com/jncraton/rebrickable-import-dumps/). You should use that project if you have access to a Linux/WSL environment.

## Dependencies

The project uses the following NuGet packages:

- CommandLineParser 
- CsvHelper to load the table data from CSV files
- Systm.Data.SQLite.Core to create and populate the SQLite database

The application also assumes that &zip will be installed on the PC it is running on.

## Outstanding

- Mirror project on GitHub
- Add a proper logging framework
- Tune performance of the data insertion into the SQLite database
- Improve error handling
- Clean up of files, etc when console app completes