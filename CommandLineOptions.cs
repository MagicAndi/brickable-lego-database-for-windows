﻿using System;
using System.Collections.Generic;
using System.Text;
using CommandLine;

namespace BuildBrickablesDatabase
{
    public class CommandLineOptions
    {
        [Option('o', "outputFile", Required = false, HelpText = "Give file path to be used to create the SQLite database.")]
        public string OutputFile { get; set; }

        [Option('d', "downloadData", Required = false, HelpText = "Force download of latest data files.")]
        public bool DownloadData { get; set; }
    }
}
